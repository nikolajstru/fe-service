import Vue from 'vue';
import Router from 'vue-router';
import Profile from './views/Profile.vue';
import Login from './views/auth/Login.vue';
import Register from './views/auth/Register.vue';
import Dashboard from './views/Dashboard.vue';
import Home from './views/Home.vue';
import Statistics from './views/Statistics.vue';
import Notifications from './views/Notifications.vue';
import Promo from './views/Promo.vue';
import PromoCreate from './views/promo-code/Create.vue';
import PromoIndex from './views/promo-code/Index.vue';
import PromoTags from './views/promo-code/Tags.vue';
import NotificationTemplates from './views/notifications/Templates.vue';
import NotificationStats from './views/notifications/Stats.vue';
import CreateNotification from './views/notifications/Create.vue';
import Dscreen from './views/dscreen/Index.vue';
import DscreenForm from './views/dscreen/Form.vue';
import KinolenteIndex from './views/kinolente-voting/Index.vue';
import KinolenteForm from './views/kinolente-voting/Create.vue';
import KinolenteEdit from './views/kinolente-voting/Edit.vue';
import BannerIndex from './views/banner-service/Index.vue';
import BannerCreate from './views/banner-service/Create.vue';
import SpecialAdmin from './views/special-admin/Index.vue';
import Categories from './views/special-admin/category/Index.vue';
import CategoryForm from './views/special-admin/category/Form.vue';
import SpecialCategoryIndex from './views/special-admin/special-category/Index.vue';
import SpecialCategoryForm from './views/special-admin/special-category/SpecialForm.vue';
import SpecprojectShowAll from './views/special-admin/special-category/ShowAll.vue';
import Page403 from './views/Page403.vue';

/* eslint-disable consistent-return */

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/statistics',
      name: 'statistics',
      component: Statistics,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/dscreen',
      name: 'dscreen-index',
      component: Dscreen,
      meta: {
        // requiresAuth: true,
        // requiresRead: true,
      },
    },
    {
      path: '/dscreen/create',
      name: 'dscreen-create',
      component: DscreenForm,
      meta: {
        // requiresAuth: true,
        // requiresRead: true,
      },
    },
    {
      path: '/dscreen/edit/:id',
      name: 'dscreen-edit',
      component: DscreenForm,
      meta: {
        // requiresAuth: true,
        // requiresRead: true,
      },
    },
    {
      path: '/kinolente-votings',
      name: 'kinolente-votings',
      component: KinolenteIndex,
      meta: {
        // requiresAuth: true,
        // requiresRead: true,
      },
    },
    {
      path: '/kinolente-voting/create',
      name: 'kinolente-voting-create',
      component: KinolenteForm,
      meta: {
        // requiresAuth: true,
        // requiresRead: true,
      },
    },
    {
      path: '/kinolente-voting/edit/:id',
      name: 'kinolente-voting-edit',
      component: KinolenteEdit,
      meta: {
        // requiresAuth: true,
        // requiresRead: true,
      },
    },
    {
      path: '/403',
      name: 'page403',
      component: Page403,
    },
    {
      path: '/notifications',
      name: 'notifications',
      component: Notifications,
      children: [
        {
          path: 'stats',
          name: 'notificationStats',
          component: NotificationStats,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'create',
          name: 'createNotification',
          component: CreateNotification,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'templates',
          name: 'notificationTemplates',
          component: NotificationTemplates,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
      ],
    },
    {
      path: '/promo',
      name: 'promo',
      component: Promo,
      children: [
        {
          path: '',
          name: 'promoIndex',
          component: PromoIndex,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'create',
          name: 'promoCreate',
          component: PromoCreate,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'tags',
          name: 'promoTags',
          component: PromoTags,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
      ],
    },
    {
      path: '/banner',
      name: 'banner-index',
      component: BannerIndex,
      meta: {
        requiresAuth: true
      },
    },
    {
      path: '/banner/create',
      name: 'banner-create',
      component: BannerCreate,
      meta: {
        requiresAuth: true
      },
    },
    {
      path: '/banner/edit/:id',
      name: 'banner-edit',
      component: BannerCreate,
      meta: {
        requiresAuth: true
      },
    },
    {
      path: '/special-admin',
      name: 'special-admin',
      component: SpecialAdmin,
      children: [
        {
          path: 'categories',
          name: 'categories',
          component: Categories,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'form',
          name: 'category-form-new',
          component: CategoryForm,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'form/:id',
          name: 'category-form',
          component: CategoryForm,
          meta: {
            requiresAuth: true,
            requiresRead: true,
          },
        },
        {
          path: 'special-categories',
          name: 'special-categories',
          component: SpecialCategoryIndex,
          meta: {
            requiresAuth: true,
            requiresSpecialAdmin: true
          },
          children: [
            {
              path: 'linked',
              name: 'specprojects-linked',
              component: SpecprojectShowAll,
              props: { view: 'linked' },
              meta: {
                requiresAuth: true,
                requiresSpecialAdmin: true
              }
            },
            {
              path: 'unliked',
              name: 'specprojects-unlinked',
              component: SpecprojectShowAll,
              props: { view: 'unlinked' },
              meta: {
                requiresAuth: true,
                requiresSpecialAdmin: true
              }
            },
            {
              path: 'create',
              name: 'specproject-create',
              component: SpecialCategoryForm,
              props: { unlinked_prop: true },
              meta: {
                requiresAuth: true,
                requiresSpecialAdmin: true
              }
            },
            {
              path: 'form/:specialID',
              name: 'special-category-form',
              component: SpecialCategoryForm,
              meta: {
                requiresAuth: true,
                requiresSpecialAdmin: true
              }
            },
          ]
        },
      ],
    },
  ],
});


router.beforeEach((to, from, next) => {
  if (to.name === 'page403' && from.name === null) {
    next({
      path: '/',
    });
  }
  const user = localStorage.getItem('USER') ? JSON.parse(localStorage.getItem('USER')) : '';
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (user) {
      if (to.matched.some(record => record.meta.requiresRead)) {
        if (user.rights && user.rights.indexOf('r') > -1
          && user.userType && user.userType.indexOf('dplusCRM') > -1) {
          next();
        } else {
          next({
            path: '/403',
          });
        }
      } else if(to.matched.some(record => record.meta.requiresSpecialAdmin)) {
        if (user.rights && user.rights.indexOf('r') > -1
          && user.userType && (user.userType.indexOf('specialAdmin') > -1 || user.userType.indexOf('dplusCRM') > -1)) {
          next();
        } else {
          next({
            path: '/403',
          });
        }
      } else {
        next();
      }
    } else {
      next({
        path: '/login',
      });
    }
  } else {
    next();
  }
});

/*

if (to.matched.some(record => record.meta.requiresRead)) {
        if (user.rights.indexOf('r') > -1) {
          next();
        } else {
          next({
            path: '/403',
          });
        }
      } else {
        next();
      }

*/

export default router;
